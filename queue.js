let collection = [];


// Write the queue functions below.
// Show all items in the stack
function print(){
	return collection;
};

// Push an element in the stack - enqueue
function enqueue(element){
	collection[collection.length] = element;
     return collection;
};

// Pop an element in the stack-dequeue
function dequeue(){
	let newArray =[];
	for(let i = 0; i<collection.length -1; i++){
		newArray[i] = collection[i+1];
	}
	collection = newArray;
     return collection;
};

// Show the top element in the stack-front
function front() {
   return(collection[0]);

};

// Return the size of the stack-size
function size(){
   return collection.length;  
};


function isEmpty(){
	if(collection.length !== 0){
		return false;
	}

};




module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty


};


/*

Required stack functions:
 - show all the items in the stack
 - push an element in the stack
 - pop an element in the stack
 - show the top element in the stack
 - return the size of the stack
 - return if there are items in the stack or not*/